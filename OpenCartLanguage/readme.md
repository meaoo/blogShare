
> 最新版本的语言包是在opencart最新英文版的基础上全新翻译而成，许多翻译的词汇与之前版本比有较大改变，目的是为了更为切合opencart的功能而准确予以表达，不当之处，恳请各位不吝赐教。
下载后解压压缩包里面有两个文件夹，admin是后台语言包，catalog文件是前台语言包。请把它们上传到你的OpenCart 网站目录根。登录到你的网店后台。
注意： 请确保你现在的OpenCart版本与将要安装的语言包版本相符

>  支持版本  3.0.0.0, 3.0.1.1, 3.0.1.2, 3.0.2.0, 3.0.3.0b


# 下载地址
[中文语言包下载地址](https://gitlab.com/meaoo/blogShare/tree/master/OpenCartLanguage)

# 安装
1. 从如下两个网站其一下载本语言包，下载后名称为: `oc_3x.ocmod.zip`，不要解压！不要解压！不要解压！
<!--more-->
2. 登录您的网站后台，打开如下路径：【extensions】->【installer】，如下界面：
![上传页面](https://cdn.meaoo.cn/18-4-18/13366996.jpg)
3. 点击上面的upload，选择刚才下载的安装包
4. 安装成功后如下提示：
![安装成功页面](https://cdn.meaoo.cn/18-4-18/27751851.jpg)

# 设置语言
1. 安装完毕后，访问后续路径【system】->【localisation】->[languages]，点击界面右上角的新增按钮，如下图所示：
![设置语言](https://cdn.meaoo.cn/18-4-18/31375727.jpg)
2. 
```
新增页面如下图所示填写相关信息：
Language Name: 简体中文
Code: zh-cn
Locale: zh-CN,zh-CN.UTF-8,zh-cn
Status: Enabled
Sort Order: 随意数字
```
![设置参数](https://cdn.meaoo.cn/18-4-18/14125079.jpg)

# 设置后台默认语言
1. 设置路径为【system】->【settings】->[Edit Store]->Local Tab Page，如下图所示：
![设置后台默认语言](https://cdn.meaoo.cn/18-4-18/38494037.jpg)
2. 保存，安装完毕


# 结束
```java
E-mail：m@meaoo.cn
QQ : 774540069
```